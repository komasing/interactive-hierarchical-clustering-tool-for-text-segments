from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import Ward
from sklearn.cluster import ward_tree
import scipy.spatial.distance as dist
import scipy.cluster.hierarchy as hier

with open("hinnavaatlus.txt",'r') as fin:
	data = []
	counter = 0
	while counter < 10:
		data.append(fin.readline().rstrip())
		counter += 1

#print data
vectorizer = TfidfVectorizer(min_df=1)
v = vectorizer.fit_transform(data)
#print vectorizer.get_feature_names()
#print v

#for i in range(1,10):
	#ward = Ward(n_clusters=i,compute_full_tree=False).fit(v.toarray())
	#print ward.labels_

#ward_t = linkage_tree(v.toarray())
#print ward_t

distMat = dist.pdist(v.toarray(),metric="cosine")
#print distMat

linkageMat = hier.linkage(distMat)

print [list(arr) for arr in list(linkageMat)]
