import os
import sys
import imp
import cherrypy
import pymongo
import ConfigParser as cp

PATH = os.path.abspath(os.path.dirname(__file__))
CONFIG_FILE = "%s/confs/server.conf"%PATH
MODULE_CLASS_FILE = "%s/confs/module_class.conf"%PATH
MODULE_PATH = "%s/modules"%PATH

MONGO_CLIENT = pymongo.MongoClient('localhost',int(sys.argv[1]))

class Root:
	def __init__(self):
		config = cp.ConfigParser()
		config.read(CONFIG_FILE)

		config.set("/","tools.staticdir.root","\"%s/static\""%PATH)
		config.set("/style.css","tools.staticfile.filename","\"%s/static/style.css\""%PATH)
		config.set("/d3.min.js","tools.staticfile.filename","\"%s/static/d3.min.js\""%PATH)
		config.set("/script.js","tools.staticfile.filename","\"%s/static/script.js\""%PATH)
		config.set("/jquery-1.11.0.min.js","tools.staticfile.filename","\"%s/static/jquery-1.11.0.min.js\""%PATH)

		with open(CONFIG_FILE,'w') as config_handler:
			config.write(config_handler)

def init_module_class_map():
	module_class_map = {}
	with open(MODULE_CLASS_FILE,'r') as module_class_handler:
		for line in module_class_handler:
			module,klass = line.rstrip().split(":")
			module_class_map[module] = klass
	return module_class_map

def add_handlers_from_map(root,mapp):
	for module in mapp:
		print "%s %s"%(module,MODULE_PATH)
		loaded_module = imp.load_source(module,"%s/%s.py"%(MODULE_PATH,module))
		klass = getattr(loaded_module,mapp[module])
		#klass = loaded_module.Test
		setattr(root,mapp[module].lower(),klass())
		
if __name__ == "__main__":

	sys.path.append(MODULE_PATH) # for dynamically imported modules imports
	root = Root()
	add_handlers_from_map(root,init_module_class_map())
	cherrypy.quickstart(root,config=CONFIG_FILE)
