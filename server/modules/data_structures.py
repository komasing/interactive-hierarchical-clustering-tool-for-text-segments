class Tree:
        def __init__(self,distance,collapsed,label,cluster,left,right,cluster_root=0):
                self.parent = None
                self.left = left
                self.right = right
                self.left.parent = self
                self.right.parent = self
                self.cluster_root = cluster_root

                self.distance = distance
                self.collapsed = collapsed
                self.label = label
                #self.label = "" # have to reload regular expressions
                self.cluster = cluster

                self.vectors_left = None
                self.vectors_right = None

		self.added_into = 0

        def __unicode__(self):
                return "{\"added_into\":%d,\"distance\":%s,\"collapsed\":%d,\"label\":\"%s\",\"cluster\":\"%s\",\"children\":[%s,%s]}"%(self.added_into,self.distance,self.collapsed,self.label,self.cluster,unicode(self.left),unicode(self.right))

class Leaf:
        def __init__(self,distance,collapsed,label,cluster,datapoint,cluster_root=0,new_node=0):
                self.datapoint = datapoint
                self.distance = distance
                self.collapsed = collapsed
                self.label = label
                self.cluster = cluster
                self.cluster_root = cluster
                self.new_node = new_node

        def __unicode__(self):
                return "{\"new_node\":%d,\"added_into\":0,\"distance\":%s,\"collapsed\":%d,\"label\":\"%s\",\"cluster\":\"%s\",\"children\":[%s]}"%(self.new_node,self.distance,self.collapsed,self.label, self.cluster,unicode(self.datapoint))

class Datapoint:
        def __init__(self,label,idx=None):
                self.label = label
                self.vector_idx = idx

        def __unicode__(self):
                return "{\"label\":\"%s\"}"%self.label
