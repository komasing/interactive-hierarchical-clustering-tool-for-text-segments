import cherrypy
import json
from hierarchical_clusterer import HierarchicalClusterer

class Adder:
	@cherrypy.expose
	def add_tegments_from_textarea(self,method,distance,into_cluster,algo="BoW TF-IDF"):
		cl = cherrypy.request.headers['Content-Length']
                rawbody = cherrypy.request.body.read(int(cl))
		#print
		#print rawbody
		#print
		data = json.loads(rawbody)
		result = HierarchicalClusterer(data["tree"]).add_tegments(data["new_tegments"],into_cluster == '1',method,distance)
		#print result
		return result

	@cherrypy.expose
	def add_tegments_from_file(self,added_tegments,tree,method,distance,into_cluster):
		new_tegments = [line.rstrip().decode("utf-8") for line in added_tegments.file.readlines()]

                result = HierarchicalClusterer(json.loads(tree)).add_tegments(new_tegments,into_cluster == '1',method,distance)
		return result
