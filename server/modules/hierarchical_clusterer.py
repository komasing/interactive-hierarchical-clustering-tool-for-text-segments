from sklearn.feature_extraction.text import TfidfVectorizer
import scipy.spatial.distance as scidist
import scipy.cluster.hierarchy as hier
import numpy as np
from dendrogram import steps_to_tree
from regex_creator import RegexCreator
from data_structures import Tree,Leaf,Datapoint
import json


class HierarchicalClusterer:
	def __init__(self,data):
		self.data = data

	def cluster_from_plain(self,method="single",metric="cosine"):
		vectorizer = TfidfVectorizer()
		normalised_bow = vectorizer.fit_transform(self.data)
		distance_matrix = scidist.pdist(normalised_bow.toarray(),metric=metric)
		linkage_matrix = hier.linkage(distance_matrix,method=method)

		return steps_to_tree(linkage_matrix,self.data)

	def get_root_label(self):
		vectorizer = TfidfVectorizer()
		normalised_bow = vectorizer.fit_transform(parse_segments_from_json(self.data))
		words = vectorizer.get_feature_names()

		norm_bow_arr = normalised_bow.toarray()
		bow_arr_bool = [[True if norm_bow_arr[i,j] else False for j in range(len(norm_bow_arr[0]))] for i in range(len(norm_bow_arr))]

		common_words_bool = reduce(list_and,bow_arr_bool)
		return [words[i] for i in range(len(words)) if common_words_bool[i]]

	def add_tegments(self, new_tegments, into_cluster, method="single", metric="cosine"):
		vectorizer = TfidfVectorizer()

		global GLOBAL_IDX
		GLOBAL_IDX = len(new_tegments)
		
		global GLOBAL_TEGMENTS
		GLOBAL_TEGMENTS = new_tegments

		N = len(new_tegments)
		
		dendro = getTree(self.data)

		normalised_bow = vectorizer.fit_transform(GLOBAL_TEGMENTS)
		arr = normalised_bow.toarray()

		for i in range(N):
			vector = arr[i]
			closest,dist = find_closest_cluster(vector,dendro,eval(method),metric,arr,into_cluster)

			datapoint = Datapoint(new_tegments[i],i)

			merge_new_and_closest(Leaf(0,0,"","Default",datapoint,0,1),closest,dist,eval(method),metric)

		return unicode(dendro)

def merge_new_and_closest(new,closest,dist,method,metric):
	parent = closest.parent
	new_node = Tree(dist,0,"","Default",new,closest)
	new_node.label = RegexCreator(new_node).get_regex_for_new_node()
	print new_node.label
	if closest == parent.left:
		parent.left = new_node
	elif closest == parent.right:
		parent.right = new_node
	else:
		print "AAAAAAAAAAAAAAAAAAAAAAAAARRRRRRRRRRRRRRRRRRRRRRRGGGGGGGGGGGGGGGGGHHHHHHHHHHHHHHHH"
	current = parent
	while current != None:
		if method == weighted:
			current.distance = method(metric,current.left.vectors_left,current.left.vectors_right,current.vectors_right)
		else:
			current.distance = method(metric,current.vectors_left,current.vectors_right)

		current = current.parent

	if closest.in_cluster:
		print "HOOAH"
		new.in_cluster = True
		current = parent
		while current.in_cluster:
			if current.label:
				current.label = RegexCreator(current).get_regex_for_new_node()
			current = current.parent
		current.added_into += 1
	else:
		new.in_cluster = False


def find_closest_cluster(to_assign,dendro,method,metric,vectors,into_cluster):
	cluster1 = [to_assign]
	closest_and_score = [None,float("inf")]
	
	def traverse(node,in_cluster):
		node.in_cluster = 1 == in_cluster
		if isinstance(node,Leaf):
			if not in_cluster or in_cluster and into_cluster:
				#node.in_cluster = False
				if method == weighted:
					dist = method(metric,vectors[node.datapoint.vector_idx],vectors[node.datapoint.vector_idx],cluster1)
				else:
					#print len(vectors)
					#print node.datapoint.vector_idx
					dist = method(metric,[vectors[node.datapoint.vector_idx]],cluster1)
				if dist < closest_and_score[1]:
					closest_and_score[1] = dist
					closest_and_score[0] = node
			return [vectors[node.datapoint.vector_idx]]
		else:
			if in_cluster and not into_cluster:
				#node.in_cluster = True
				#print [in_cluster]
				if not node.vectors_left:
					cluster21 = traverse(node.left, in_cluster)
					cluster22 = traverse(node.right, in_cluster) # also missing node.vectors_right
				else:
					cluster21 = node.vectors_left
					cluster22 = node.vectors_right

				merged = cluster21+cluster22
			else:
				#node.in_cluster = False
				#print [in_cluster,"ei ole klastris"]
				#cluster21 = traverse(node.left,node.cluster_root)
                                #cluster22 = traverse(node.right,node.cluster_root)
				cluster21 = traverse(node.left,node.collapsed)
                                cluster22 = traverse(node.right,node.collapsed)
				merged = cluster21+cluster22
				if method == weighted:
					dist = method(metric,cluster21,cluster22,cluster1)
				else:
					dist = method(metric,merged,cluster1)
				if dist < closest_and_score[1]:
					closest_and_score[1] = dist
					closest_and_score[0] = node

			node.vectors_left = cluster21
			node.vectors_right = cluster22
			return merged

	traverse(dendro,0)
	return closest_and_score

def list_and(list1,list2):
	n = len(list1)
	return [list1[i] & list2[i] for i in range(n)]

def parse_segments_from_json(data):
	tree = json.loads(data)
	stack = [tree]
	segments = []

	while len(stack) > 0:
		node = stack.pop()
		children = node["children"]
		if len(children) == 1:
			segments.append(children[0]["label"])
		else:
			stack.append(children[0])
			stack.append(children[1])
	
	return segments

############################################
"""
class Tree:
	def __init__(self,distance,collapsed,label,cluster,left,right,cluster_root=0):
		self.parent = None
		self.left = left
		self.right = right
		self.left.parent = self0
		self.right.parent = self
		self.cluster_root = cluster_root

		self.distance = distance
		self.collapsed = collapsed
		self.label = label
		#self.label = "" # have to reload regular expressions
		self.cluster = cluster

		self.vectors_left = None
		self.vectors_right = None

		self.added_into = 0

	def __unicode__(self):
		return "{\"added_into\":%d,\"distance\":%s,\"collapsed\":%d,\"label\":\"%s\",\"cluster\":\"%s\",\"children\":[%s,%s]}"%(self.added_into,self.distance,self.collapsed,self.label,self.cluster,unicode(self.left),unicode(self.right))

class Leaf:
	def __init__(self,distance,collapsed,label,cluster,datapoint,cluster_root=0,new_node=0):
		self.datapoint = datapoint
		self.distance = distance
		self.collapsed = collapsed
		self.label = label
		self.cluster = cluster
		self.cluster_root = cluster
		self.new_node = new_node

        def __unicode__(self):
                return "{\"new_node\":%d,\"added_into\":0,\"distance\":%s,\"collapsed\":%d,\"label\":\"%s\",\"cluster\":\"%s\",\"children\":[%s]}"%(self.new_node,self.distance,self.collapsed,self.label, self.cluster,unicode(self.datapoint))

class Datapoint:
	def __init__(self,label,idx=None):
		self.label = label
		if idx == None:
			self.vector_idx = GLOBAL_IDX
			global GLOBAL_TEGMENTS
			GLOBAL_TEGMENTS.append(self.label)
			global GLOBAL_IDX
			GLOBAL_IDX += 1
		else:
			self.vector_idx = idx

	def __unicode__(self):
		return "{\"label\":\"%s\"}"%self.label
"""

def getTree(data):
	def treefier(node):
		if "children" in node:
			children = node["children"]
			if len(children) == 1:

				datapoint = Datapoint(children[0]["label"])

                                global GLOBAL_IDX
				datapoint.vector_idx = GLOBAL_IDX
                        	global GLOBAL_TEGMENTS
                        	GLOBAL_TEGMENTS.append(datapoint.label)
                        	GLOBAL_IDX += 1

				return Leaf(node["distance"],node["collapsed"],node["label"],node["cluster"],datapoint, 1 if "cluster_root" in node else 0)
			else:
				left = treefier(node["children"][0])
				right = treefier(node["children"][1])
				print node["label"]
				return Tree(node["distance"],node["collapsed"],node["label"],node["cluster"],left,right, 1 if "cluster_root" in node else 0)
	return treefier(data)
	

############################################

def single(metric,cluster1,cluster2):
	min_dist = float("inf")

	for i in range(len(cluster1)):
		for j in range(len(cluster2)):
			dist = scidist.pdist([cluster1[i],cluster2[j]],metric)[0]
			if dist < min_dist:
				min_dist = dist
	return min_dist

def complete(metric,cluster1,cluster2):
	max_dist = float("-inf")
	for i in range(len(cluster1)):
		for j in range(len(cluster2)):
			dist = scidist.pdist([cluster1[i],cluster2[j]],metric)[0]
			if dist > max_dist:
				max_dist = dist
	return max_dist

def average(metric,cluster1,cluster2):
	total = 0
	for i in range(len(cluster1)):
		for j in range(len(cluster2)):
			total += scidist.pdist([cluster1[i],cluster2[j]],metric)[0]
	return total/(len(cluster1)*len(cluster2))

def weighted(metric,cluster11,cluster12,cluster2):
	return (scidist.pdist([[cluster11],cluster2],metric)[0]+scidist.pdist([[cluster12],cluster2],metric)[0])/2

#distance_matrix = dist.pdist(np.array([[1,2,3],[2,3,4]]),metric="euclidean")
#print distance_matrix
#linkage_matrix = hier.linkage(distance_matrix,method="single")
#print linkage_matrix
