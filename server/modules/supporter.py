import cherrypy
import algorithms
import server
import json
import datetime

from hierarchical_clusterer import HierarchicalClusterer
from regex_creator import RegexCreator

class Supporter:
	@cherrypy.expose
	def get_methods(self):
		algos = [algorithms.BoW_TF_IDF()]
		return "{\"algorithms\":[%s]}"%','.join([str(algo) for algo in algos])

	@cherrypy.expose
	def get_db_documents(self):
		collections = server.MONGO_CLIENT.clustering.clustering
		documents = collections.distinct("name")
		print documents
		return json.dumps(documents)

	@cherrypy.expose
	def clear_db(self):
		server.MONGO_CLIENT.clustering.clustering.remove()

	@cherrypy.expose
	def get_node_label(self):
		cl = cherrypy.request.headers['Content-Length']
		json_str = cherrypy.request.body.read(int(cl))
		
		hc = HierarchicalClusterer(json_str)
		result = u"%s"%hc.get_root_label()

		result = json.dumps(result)
		print result
		return result

	@cherrypy.expose
	def get_node_label2(self):
		cl = cherrypy.request.headers['Content-Length']
		json_str = cherrypy.request.body.read(int(cl))

		tree = json.loads(json_str)
		rc = RegexCreator(tree)
		return rc.get_regex()

	@cherrypy.expose
	def upload_svg(self):
		cl = cherrypy.request.headers['Content-Length']
		svg_str = cherrypy.request.body.read(int(cl))
		timestamp = datetime.datetime.today().strftime("%Y_%m_%d_%H_%M_%S")
		
		with open(server.PATH + '/images/' + timestamp,'w') as file:
			file.write(svg_str)

		return timestamp

		#svg = StringIO.StringIO(content)
		#return cherrypy.lib.static.serve_fileobj(svg, disposition="attachment",
		#					content_type=".txt",name=str(document_name))

	@cherrypy.expose
	def download_svg(self,filename,svg_key):
		svg_file = open(server.PATH + "/images/" + str(svg_key))
		
		return cherrypy.lib.static.serve_fileobj(svg_file, disposition="attachment",
                                                       content_type=".svg",name=str(filename) + ".svg")

