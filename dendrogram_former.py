class Dendrogram:
	def __init__(self,formation_steps=None,leaf_labels=None,initial_idx=0,json_data=None):
		if formation_steps and leaf_labels:
			step_map = {}
			fst_super_cluster = initial_idx+len(formation_steps)
			for delta_idx in range(len(formation_steps)):
				step_map[fst_super_cluster+delta_idx] = formation_steps[delta_idx]

			
		elif json_data:
			pass
		else:
			raise DataMissing()

class DataMissing(Exception):
	pass

class IncorrectInput(Exception):
	pass

class Cluster:

	def __init__(self,fst_child,snd_child,label):
		self.fst_child = fst_child
		self.snd_child = snd_child
		self.label = label

class Leaf:
	
